scanlogd (2.2.7-0.3) unstable; urgency=medium

  * debian/scanlogd.init:
    + Drop if clause, also let root own RDIR itself. Document this in a comment.
      (Closes: #984590).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 05 Mar 2021 17:58:38 +0100

scanlogd (2.2.7-0.2) unstable; urgency=medium

  * debian/scanlogd.init:
    + RDIR/empty must not be writeable by the user that scanlogd runs as.
      (Closes: #984590).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 05 Mar 2021 15:36:28 +0100

scanlogd (2.2.7-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  * New upstream release. (Closes: #945478).
  * debian/patches:
    + Add 2001_Makefile-default-to-linux-capturing-backend.patch. Let Makefile
      default to 'linux' backend.
    + Add 2002_allow-pkg-maintainer-flags.patch. Allow injecting maintainer
      CFLAGS, honour CPPFLAGS, drop '-s' from LDFLAGS. (Closes: #437957).
    + Add 1001_drop-BSD-SOURCE-define.patch. Drop deprecated _BSD_SOURCE macro
      definition.
    + Add 2003_use-home-as-chroot-dir.patch. Adapt SCANLOGD_CHROOT to a
      directory that does exist after package installation.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation, bump to DH compat level 13.
  * debian/source/format:
    + Add file, switch to '3.0 (quilt)'.
  * debian/rules:
    + Add optional get-orig-source target (for personal convenience).
    + Convert to short DH style. (Closes: #437957).
    + Enable all hardening build flags.
  * debian/install:
    + Add file; ensure installation of scanlogd daemon executable into bin:pkg.
  * debian/watch:
    + Switch to format version 4.
  * debian/dirs:
    + Drop file, not required.
  * debian/manpages:
    + Add file. Use upstream-provided scanlogd.8 as man page.
  * debian/docs:
    + Drop file. It referenced an old license file. Upstream license has changed
      meanwhile.
  * maintainer scripts:
    + Don't ignore errors, switch on strict mode. (Closes: #866273).
    + Drop manual evocation of update-rc.d. Handled by DH these days.
  * debian/postinst:
    + Don't fail if '/home/scanlogd' is still present and can't be removed.
  * debian/control:
    + Improve SYNOPSIS and LONG_DESCRIPTION.
    + Switch to linux-any architectures for now. (Closes: #770303).
    + Bump to Standards-Version: 4.5.1.
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Add Vcs-*: fields.
    + Add D: lsb-base.
  * debian/scanlogd.init:
    + Source LSB init functions.
    + Also stop daemon for runlevels 0 and 6.
    + Implement 'status' option.
    + Use /run/ rather than /var/run. Create additional empty subdirectory in
      RUNDIR.
  * debian/copyright:
    + Rewrite entire file. Fix wrong license reference. (Closes: #945478).
  * debian/changelog:
    + White-space cleanup.

  [ Cristian Ionescu-Idbohrn ]
  * debian/scanlogd.init:
    + Remove end-of-line whitespace.
    + Remove magic shebang SPACE.
    + Reuse variables. Copy/paste is less maintainable.
    + 'chmod' seems meaningless, as 'umask 022' guarantees '0755'; 'chown'
      may be more meanigful, as that was what was done before. (Closes:
      #654310).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 22 Feb 2021 09:56:12 +0100

scanlogd (2.2.5-3.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Bumped DH level to 10.
  * debian/compat: created. (Closes: #827810)
  * debian/control:
      - Added the ${misc:Depends} variable to Depends field.
      - Bumped Standards-Version to 3.9.8.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Thu, 22 Dec 2016 17:20:51 -0200

scanlogd (2.2.5-3.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix handling of /var/run/scanlogd, thanks to Thomas Goirand.
    (Closes: #689903)
  * Use /var/lib/scanlogd instead of /home/scanlogd as the non-existing home
    directory for the scanlogd user, thanks to lintian.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 08 Nov 2014 23:25:20 +0100

scanlogd (2.2.5-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Set adduser in postinst not to create /home/scanlogd and remove it if it
    exists. Ssolves unowned files left on system after purge (Closes: #668722)

 -- Scott Kitterman <scott@kitterman.com>  Thu, 05 Jul 2012 23:54:56 -0400

scanlogd (2.2.5-3) unstable; urgency=low

  * fix init.d script when /var/run is a tmpfs
    thanks to Mathias Bauer (closes: #654310)

 -- Michael Vogt <mvo@debian.org>  Tue, 03 Jan 2012 15:22:23 +0100

scanlogd (2.2.5-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Added LSB formatted dependency info in init.d script (closes: #468133)
  * Added Homepage control field
  * Added watch file

 -- Peter Eisentraut <petere@debian.org>  Thu, 03 Apr 2008 12:18:15 +0200

scanlogd (2.2.5-2.1) unstable; urgency=medium

  * Non-maintainer upload during BSP.
  * Substitute CLK_TCK with CLOCKS_PER_SEC in scanlogd.c and P53-13 to avoid
    FTBFS with new glibc. (Closes: #421085).
  * Use now dh_installman instead of dh_installmanpages

 -- Mario Iseli <admin@marioiseli.com>  Thu, 17 May 2007 20:30:11 +0200

scanlogd (2.2.5-2) unstable; urgency=low

  * use invoke-rc.d (closes: #338394)
  * added /var/run/scanlogd and make sure scanlogd chroots into it

 -- Michael Vogt <mvo@debian.org>  Tue, 25 Jul 2006 20:03:42 +0200

scanlogd (2.2.5-1) unstable; urgency=low

  * New upstream release (closes: #262056)
  * home of the scanlogd user is now /usr/lib/scanlogd (scanlogd chroots
    into it)

 -- Michael Vogt <mvo@debian.org>  Mon,  2 Aug 2004 14:00:28 +0200

scanlogd (2.2-1) unstable; urgency=low

  * new upstream release, closes: #94119

 -- Michael Vogt <mvo@debian.org>  Tue, 27 Mar 2001 09:37:02 +0200

scanlogd (2.1-1) unstable; urgency=low

  * new upstream
  * close: Bug #69771

 -- Michael Vogt <mvo@debian.org>  Mon, 28 Aug 2000 14:25:39 +0200

scanlogd (1.3-7) unstable; urgency=low

  * close: Bug #69929,#70015 (this time, I got it right)

 -- Michael Vogt <mvo@debian.org>  Sun, 27 Aug 2000 21:47:04 +0200

scanlogd (1.3-6) unstable; urgency=low

  * fix: Bug #69929 (thanks to Antti-Juhani Kaijanaho)

 -- Michael Vogt <mvo@debian.org>  Sat, 26 Aug 2000 14:45:51 +0200

scanlogd (1.3-5) unstable; urgency=low

  * fix: Bug #69771 (thanks to Joey Hess)

 -- Michael Vogt <mvo@debian.org>  Wed, 23 Aug 2000 10:18:37 +0200

scanlogd (1.3-4) frozen unstable; urgency=low

  * fixed a stuid typo (Bug #58146). Thanks to Peter Naulls

 -- Michael Vogt <mvogt@acm.org>  Tue, 30 May 2000 12:35:42 +0200

scanlogd (1.3-3) unstable; urgency=low

  * fixed bug #64862 (thanks to Joost Yervante Damad
                      <joost.damad@siemens.atea.be>

 -- Michael Vogt <mvogt@acm.org>  Tue, 30 May 2000 12:29:56 +0200

scanlogd (1.3-2) unstable; urgency=low

  * fixed bug in postinst/prerm/postrm

 -- Michael Vogt <mvogt@acm.org>  Mon, 16 Feb 2000 13:00:00 +0100

scanlogd (1.3-1) unstable; urgency=low

  * Initial Release.

 -- Michael Vogt <mvogt@acm.org>  Sun, 19 Dec 1999 16:18:56 +0100
